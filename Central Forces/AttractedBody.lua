AttractedBody = class()

function AttractedBody:init(name, x, y, mass, centralBody, radius)
    -- you can accept and set parameters here
    self.pBody = physics.body(CIRCLE, radius or math.random(5, 25)) --The physics body, a CIRCLE
    --initial position is relative to the central body
    self.pBody.x = x + centralBody.x
    self.pBody.y = y + centralBody.y
    self.pBody.position = vec2(self.pBody.x, self.pBody.y)
    self.pBody.mass = mass
    self.pBody.friction = 0
    self.pBody.linearDamping = 0
    self.pBody.angularDamping = 0
    self.pBody.gravityScale = 0
    self.centralBody = centralBody
    self.pBody.linearVelocity = self:compute_initial_speed()
    self.forceFactor = 1
    self.TD = TrajectoryDrawer(self.pBody, 500, 0.05)
    parameter.number(name.."ForceFactor", 100, 1000, 500, function(x) self:set_factor(x) end)
end

function AttractedBody:draw()
    -- Codea does not automatically call this method
    ellipse(self.pBody.x, self.pBody.y, self.pBody.radius) 
    self.TD:draw()
end

function AttractedBody:touched(touch)
    -- Codea does not automatically call this method
end

-- compute the initial speed making the speed vector perpendicular to
-- the line between the centers of mass
function AttractedBody:compute_initial_speed()
    --compute vector between bodies
    local vec_AB = self.pBody.position -- self.centralBody.position
    vec_AB = vec_AB:normalize()
    vec_AB = vec_AB:rotate90() --perpendicular unit vector
    --random factor to multiply the unit vector
    vec_AB = math.random(50, 150) * vec_AB
    return vec_AB
end

function AttractedBody:set_factor(value)
    self.forceFactor = value
end

function AttractedBody:run()
    --compute distance between centers
    local r = self.centralBody.position - self.pBody.position
    --Compute force between bodies based on gravitational attraction
    local force = (self.forceFactor*self.pBody.mass/(r:lenSqr()))*r
    self.pBody:applyForce(force)
    self:draw()
end
