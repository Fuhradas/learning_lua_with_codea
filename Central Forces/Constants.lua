R_EARTH = 6378 -- Earth radius in kms, used to adimensionalize
MU_EARTH = 398601 -- Earth gravitational parameter in km^3/s^2
TU = math.sqrt(MU_EARTH / R_EARTH) -- Time Unit
