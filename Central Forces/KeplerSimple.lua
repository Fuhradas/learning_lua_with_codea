--First implementation only valid for elliptical orbits

KeplerSimple = class()

-- Canonical units are used here, DU = 6378 km, TU ~= 809 s, speed = DU/TU

--The contructor receives the a (in earth radiuses, that is a = 1 means a = 6378 km
function KeplerSimple:init(a, e, omega, nu, planet)
    -- you can accept and set parameters here
    self.a = a --dimensionless semi-major axis
    self.e = e --eccentricity
    self.omega = omega --argument of periapsis in radians
    self.nu = nu --initial true anomaly
    self.p = self.a*(1 - (self.e * self.e)) --dimensionless semiparameter
    self.n = 1/math.sqrt(self.a*self.a*self.a) --dimensionless mean motion
    -- initialization of position and speed vectors in PQW reference frame
    self.r = vec2(0,0) --from focus to the current orbital position (r_p, r_q)
    self.v = vec2(0,0) -- speed vector (v_p, v_q)
    self:r_v_from_COE()
    self.M =  nu2mean(self.nu, self.e, self.p, self.r:len())-- initial mean anomaly
    -- position vector in Codea frame (needed for the trajectory drawer)
    self.position = vec2(0, 0)
    self.planet = planet
    self.TD = TrajectoryDrawer(self, 500, 0.1)
    self.color = color(math.random(0, 255), math.random(0, 255), math.random(0, 255))
end

function KeplerSimple:draw()
    pushMatrix()
    pushStyle()
    stroke(self.color)
    translate(self.planet.x, self.planet.y)
    rotate(math.deg(self.omega))
     -- draw in the PQW frame
    self.TD:draw()
    local scaleFactor = 100
    self.position = scaleFactor*self.r
    ellipse(self.r.x*scaleFactor, self.r.y*scaleFactor, 10)
    strokeWidth(3)
    line(0, 0, self.r.x*scaleFactor, self.r.y*scaleFactor)
    
    --draw speed vector
    pushMatrix()
    translate(self.r.x*scaleFactor, self.r.y*scaleFactor)
    draw_vector(self.v.x*scaleFactor, self.v.y*scaleFactor)
    popMatrix()
    
    popStyle()
    popMatrix()
    
end

function KeplerSimple:touched(touch)
end

function KeplerSimple:run()
    -- Solve Kepler's Problem
    self:kepler_problem(DeltaTime)
    -- Draw orbit
    self:draw()
end

--Compute r and v from orbital elements
function KeplerSimple:r_v_from_COE()
    local s_nu = math.sin(self.nu)
    local c_nu = math.cos(self.nu)
    local denom = 1 + (self.e * c_nu)
    --compute radiovector
    self.r.x = self.p*c_nu / denom
    self.r.y = self.p*s_nu / denom
    --compute speed vector
    local v_inv_denom = math.sqrt(self.p) --inverse denominator so we can multiply
    self.v.x = v_inv_denom*(-s_nu)
    self.v.y = v_inv_denom*(self.e + c_nu)
end

function KeplerSimple:kepler_problem(_deltaTime)
    -- compute mean anomaly increment
    self.M = self.M + self.n * _deltaTime
    local E = solve_kepler_E_ellip(self.M, self.e)
    self.nu = anomaly2nu(E, self.e)
    -- update r and v
    self:r_v_from_COE()
end
