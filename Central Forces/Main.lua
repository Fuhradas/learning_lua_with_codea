-- CentralForces

-- Use this function to perform your initial setup
function setup()
    parameter.number("CentralBodyMass", 100, 10000, 1000)
    --Create a body in the center of the screen that will be static and act as an attractor
    centralBody = physics.body(CIRCLE, 50)
    centralBody.type = STATIC
    centralBody.mass = CentralBodyMass
    centralBody.x = WIDTH*0.75
    centralBody.y = HEIGHT*0.5
    centralBody.position = vec2(centralBody.x, centralBody.y)
    testBody=AttractedBody("Sat1", 150, 150, 5, centralBody, 20)
    orbit1 = KeplerSimple(3, 0.7, 0.0, math.pi, centralBody)
    orbit2 = KeplerSimple(2.5, 0.1, 0.0, math.pi*0.5, centralBody)
    orbit3 = KeplerSimple(2.5, 0.7, 0.0, math.pi*0.5, centralBody)
    orbit4 = KeplerSimple(1, 0.0, 0.0, -math.pi, centralBody)
    orbits = {orbit1, orbit2, orbit3, orbit4}
    FPS = 0.0
    counter = 0.0
    --parameter.watch("FPS")
    cycle_counter = 0
end

-- This function gets called once every frame
function draw()
    --[[
    if(counter > 0.25) then
        FPS = 1/DeltaTime
        counter = 0.0
    else
        counter = counter + DeltaTime
    end 
    ]]
    FPS = 1/DeltaTime
    if(FPS < 30 ) then
        print("FPS below 30, FPS = "..FPS)
    end
    -- This sets a dark background color 
    background(40, 40, 50)

    -- This sets the line thickness
    strokeWidth(5)
    ellipse(centralBody.x, centralBody.y, 2*centralBody.radius)
    --line(centralBody.x, centralBody.y, testBody.pBody.x, testBody.pBody.y)
    --testBody:run()
    
    for _,orbit in pairs(orbits) do
        orbit:run()
    end
    
    -- Do your drawing here
    
end

