-- Several functions used often in Orbital Mechanics

--Solve Kepler's equation on the excentric anomaly for elliptic orbits
function solve_kepler_E_ellip(_M, _e)
    local M = _M
    local e = _e
    local E = 0.0
    local E_n --To solve the equation
    local tolerance = 1.0
    local epsilon = 1.0e-6
    local i = 1 --max iterations variable
    if ((-math.pi<M and M<0) or (M>math.pi)) then
	    E = M - e;
    else
        E = M + e;
    end

    -- iterate until an acceptable tolerance is reached or the maximum number of iterations is reached
    while (tolerance > epsilon or i>15) do
        E_n = E + (( (M-E) + (e*math.sin(E)) )/(1-(e*math.cos(E)) ));
        tolerance = math.abs(E_n-E);
        --For debugging purposes ONLY
        --print("Tolerance \tE_n \t \tE \t \tIteration")
        --print(tolerance.."\t"..E_n.."\t"..E.."\t \t"..i)
        E = E_n
        i = i + 1
    end
    return E_n
    
end

-- Compute E, B or H from true anomaly and eccentricity
function nu2anomaly (_nu, _e)
    local nu = _nu
    local e = _e
    local s_nu = math.sin(nu)
    local c_nu = math.cos(nu)
    local anomaly = 0.0 --Initialize anomaly
    if (e<1) then --Elliptical case
        local s_anomaly = ( s_nu*math.sqrt(1 - e*e) ) / (1 + e*c_nu)
        local c_anomaly = (e + c_nu) / (1 + e*c_nu)
        --We use atan2 to compute cuadrant check. atan2(y, x) ----> angle
        anomaly = math.atan(s_anomaly, c_anomaly)
    elseif (e>1) then --Hyperbolic case
        local c_anomaly = ( (e + c_nu) / (1 + e*c_nu) )
        local s_anomaly = ( s_nu*math.sqrt(e*e - 1) ) / (1 + e*c_nu)
        anomaly = atanh(s_anomaly / c_anomaly)
    else --Parabolic case
        anomaly = math.tan(nu / 2)
    end

    return anomaly
end

  -- Compute nu from E, B or H
  -- If not given, p and r equal 1.0, since they are only needed in the parabolic case
  function anomaly2nu (_anomaly, _e, _p, _r)
    local anomaly = _anomaly
    local e = _e
    local p = _p or 1.0
    local r = _r or 1.0
    local nu = 0.0; --Initialize nu
    if(e<1) then
        local c_anomaly = math.cos(anomaly)
        local s_anomaly = math.sin(anomaly)
        local c_nu = (c_anomaly - e) / (1 - e*c_anomaly)
        local s_nu = ( s_anomaly*math.sqrt(1 - e*e) ) / (1 - e*c_anomaly)
        nu = math.atan(s_nu, c_nu)
    elseif(e>1) then
        local c_anomaly = math.cosh(anomaly)
        local s_anomaly = math.sinh(anomaly)
        local c_nu = (c_anomaly - e) / (1 - e*c_anomaly)
        local s_nu = ( -s_anomaly*math.sqrt(e*e - 1) ) / (1 - e*c_anomaly)
        nu = math.atan(s_nu , c_nu)
    else
        local c_nu = (p*anomaly)/r
        local s_nu = (p-r)/r
        nu = math.atan(s_nu, c_nu)
    end
    
    return nu

end

function nu2mean(_nu, _e, _p, _r)
    local nu = _nu
    local e = _e
    local p = _p or 1.0
    local r = _r or 1.0
    local E = nu2anomaly(nu, e, p, r)
    local M = 0.0
    if (e<1) then
    	M = E - (e*math.sin(E))
    elseif (e == 1) then
	   M = E +(E*E*E/3.0)
    elseif (e > 1) then
	   M = e*math.sinh(E) - E
    end

    return M;
end

-- Since Codea does not have the inverse of hyperbolic functions in its math library
-- we have to implement them
--All these functions should be encapsulated in a table called amath (Advanced Math) so
--they could be called like amath.atanh(x)

function atanh(_x)
    local x = _x
    local result = 0.0
    result = 0.5 * math.log((1+x)/(1-x))
    return result
end

function draw_vector(_x, _y)
    local x = _x
    local y = _y
    local vector = vec2(x, y)
    line(0, 0, x, y) --main vector line
    -- Draw the arrow tip
    pushMatrix()
    pushStyle()
    strokeWidth(3)
    --Translate to the vector's tip
    translate(x, y)
    --Rotate the frame so the vector is colineal with the new x axis
    rotate(math.deg(math.atan(y, x)))
    local arrowFactor = 0.1 -- arrow's length is 10% the vector's length
    local r = vector:len()
    local x_arrow = -r*math.cos(math.rad(30))*arrowFactor
    local y_arrow = r*math.sin(math.rad(30))*arrowFactor
    line(0,0, x_arrow, y_arrow)
    line(0,0, x_arrow, -y_arrow)
    popStyle()
    popMatrix()
end


