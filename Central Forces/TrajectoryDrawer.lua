TrajectoryDrawer = class()

function TrajectoryDrawer:init(body, maxSize, sampleTime)
    -- list containing vec2
    self.points = {}
    --Maximum size that the list can have
    self.maxSize = maxSize
    --time between samples
    self.sampleTime = sampleTime or 0.25
    --physical body whose trajectory will be drawn
    self.body = body
    self.timer = os.clock() --timer to trigger the sampling
end

function TrajectoryDrawer:store_point()
    if(tableLength(self.points) > self.maxSize) then
        table.remove(self.points) --remove the last point
        table.insert(self.points, 1, self.body.position) -- insert current position
    else --if maxSize has not been reached yet
        table.insert(self.points, 1, self.body.position)
    end
end

function TrajectoryDrawer:is_time_to_store()
    if((os.clock() - self.timer) > self.sampleTime) then
        self.timer = os.clock()
        return true
    else
        return false
    end
end

function TrajectoryDrawer:run()
    if(self:is_time_to_store()) then
        self:store_point()
    end
end

function TrajectoryDrawer:draw()
    self:run()
    if (tableLength(self.points)> 0) then
       for i,v in ipairs(self.points) do
            if(i <= self.maxSize) then
               ellipse(v.x, v.y, 3) 
            end
        end
    end
end

function TrajectoryDrawer:touched(touch)
    -- Codea does not automatically call this method
end

function TrajectoryDrawer:set_max_size(new_size)
    self.maxSize = new_size
end

function tableLength(T)
  local count = 0
  for _ in pairs(T) do count = count + 1 end
  return count
end