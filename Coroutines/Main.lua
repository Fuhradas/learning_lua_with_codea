function setup()
    --create the coroutine
    local co = coroutine.create(function() print("Hello Coroutine!") end)
    print("Coroutine =", co)
    print("co type is", type(co))
    --The coroutine hasn't started yet, so it is suspended
    print("co status is", coroutine.status(co))
    print("Resuming Coroutine!")
    --Resume the coroutine, and since it has ONLY ONE statement, it will finish
    coroutine.resume(co)
    --Since the coroutine has finished, its state will be "dead"
    print("Coroutine status is", coroutine.status(co))
    
    parameter.action("Clear Output", output.clear)
    
    co2 = coroutine.create(DoTitanicTask) -- <-DoTitanicTask is the body function
end

function draw()
    local a, b, c, d
    
if (coroutine.status(co2) ~= "dead") then
        --The arguments passed to resume will be passed to the body function
        --Resume will return "true" as long as the body function hasn't finished
        -- or no errors have occurred, otherwise it will return false.
        --Apart from returning "true" if everything is going fine, it will return
        --the arguments passed to the "yield" method
        --So, in the line below, a will either be "true" or "false", and b,c and d
        -- will be "not nil" depending on whether "yield" receives any arguments
        --or not
        a, b, c, d = coroutine.resume(co2, "arg1")
        --The last time the coroutine is resumed, b, c and d will be nil because
        --the coroutine will not yield. If the printing statements were put before
        --the "resume" call, the would be nil on the first run.
        print("a =", a)
        print("b =", b)
        print("c =", c)
        print("d =", d)
    end
    
end

function DoTitanicTask(a, b)

    for i=1,10 do
        
        --print a and b if they are not nil
        if(a) then
            print(a)
        end
        
        if(b) then
            print(b)
        end
    
        print("Beginning hard work for the", i, "time")
        for j=1,100000 do
            local x = math.sqrt(j)
        end
        --"yield" suspends the execution of the CALLING routine and its arguments are
        -- passed as extra results to the "resume" method
        -- so in the line below we are passing three additional arguments to "yield" 
        local args = coroutine.yield(i, math.sqrt(i), i*i)
        --"yield" will also return any arguments passed in the "resume" call
        if(args) then 
            print("coroutine.yield returned "..args)
        end
    end
    print("Titanic task finished!")
end
