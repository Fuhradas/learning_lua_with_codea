-- Metatables

-- Use this function to perform your initial setup
function setup()
    print("Hello World!")
    local other = {foo = 3}
    local t = {}
    local mt = {}
    t = setmetatable({}, {__index=other})
    print(t.foo)
    print(t.var)
    --We can either call Vector(x,y) or Vector.new(x,y)
    --This is thanks to the last line in the Vector file (more comments there)
    a = Vector(3,5)
    b = Vector.new(4,-5)
    c = a + b
    local angle = 90
    print("a = ",a)
    print("a rotated "..angle.." degrees  = \n", a:rotated(math.rad(angle)))
    print("b = ",b)
    print(c:normalize())
    -- The following lines should print the same memory address since
    -- all the vectors have the same metatable
    print(getmetatable(a))
    print(getmetatable(b))
    print(getmetatable(c))
end

-- This function gets called once every frame
function draw()
    -- This sets a dark background color 
    background(40, 40, 50)

    -- This sets the line thickness
    strokeWidth(5)

    -- Do your drawing here
    
end

