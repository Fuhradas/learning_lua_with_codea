Axes = class()

--[[The axes class can draw as many plots as those stored in its table

]]

-- TBD
-- Find a way of scaling pixels to real axis values, i.e. 100 pixels = 1 unit
-- Add the option to scale by pinching
function Axes:init(x, y)
    -- x and y coordinates for the image center
    self.x = x
    self.y = y
    self.xSize = 300 -- x axis size in pixels
    self.ySize = self.xSize
    self.imageFactor = 4./3. --Factor used to scale the image containing the axes
    self.plottingInterval = 0.5 -- step taken into the x axis to draw a new point
    self.img = image(self.xSize*self.imageFactor, self.ySize*self.imageFactor)
    -- Table that will hold the functions to be plotted
    self.staticPlots = {}
    self.dynamicPlots = {}
end

function Axes:run()
    self:draw()
    self:touched(CurrentTouch)
end

function Axes:draw()
    --Draw axes
    local axisLength = self.xSize
    sprite(self.img, self.x, self.y)
    setContext(self.img) -- Set the image as the "drawing board"
    background(0, 0, 0, 0)
    translate(0.1*self.img.width, 0.1*self.img.height)
    --Draw y axis (before we move to the horizontal reference to half the height of the image)
    line(0, 0, 0, axisLength)
    text("Y", 0, axisLength + 2*fontSize())
    translate(0, 0.5*axisLength)
    --Draw x axis
    line(0, 0, axisLength, 0)
    text("X", axisLength + 2*fontSize(), 0)
    -- draw plots
    self:draw_plots()    
    setContext() -- Go back to the original screen
end

function Axes:touched(touch)
    -- Return true if the touch is inside the img region
    if(touch.state ~= ENDED) then
        if((touch.x > self.x - self.img.width*0.5)
            and touch.x < (self.x + self.img.width*0.5)
            and (touch.y > self.y - self.img.height*0.5)
            and touch.y < (self.y + self.img.height*0.5)) then
            self.x = self.x + touch.deltaX
            self.y = self.y + touch.deltaY
        end
    end
end

function Axes:add_plot(_plot)
    --insert _plot in the last position of the plots table
    if(_plot.isDynamic) then
        table.insert(self.dynamicPlots, _plot)
        -- set the dynamic plot buffer size based on the x axis size
        _plot.bufferSize = math.floor(self.xSize / self.plottingInterval)
    else
        table.insert(self.staticPlots, _plot) 
    end
end

function Axes:draw_plots()
    self:draw_static_plots()    
    self:manage_dynamic_plots()
end

-- It is called "manage" instead of "draw" because this function does TWO things
-- 1) update the buffer for each one of the dynamic functions
-- 2) plot the entire buffer
function Axes:manage_dynamic_plots()
    pushStyle()
    --dynamicPlots will be plotted in green
    strokeWidth(2)
    for _, v in ipairs(self.dynamicPlots) do
        --update the buffer
        v:update_buffer()
        --set the color
        stroke(v.color)
        --iterate over the buffer and increase x by the plottingInterval on every iteration
        local x = 0
        for _, b in ipairs(v.buffer) do --b is the value inside the buffer
            ellipse(x, b, 3)
            x = x + self.plottingInterval
        end
    end
    popStyle()
end

function Axes:draw_static_plots()
    pushStyle()
    strokeWidth(2)
    --draw the static plots iterating over the x axis
    for x = 0, self.xSize, self.plottingInterval do
        --draw a point for the y value returned by each function
        for _, v in ipairs(self.staticPlots) do
            -- draw the point corresponding to the function value
            ellipse(x, v:value(x), 3) 
        end
    end
    popStyle()
end


