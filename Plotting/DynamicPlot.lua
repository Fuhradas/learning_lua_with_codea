DynamicPlot = class(funcObject)

function DynamicPlot:init(x)
    -- you can accept and set parameters here
    self.x = x
end

function DynamicPlot:draw()
    -- Codea does not automatically call this method
end

function DynamicPlot:touched(touch)
    -- Codea does not automatically call this method
end
