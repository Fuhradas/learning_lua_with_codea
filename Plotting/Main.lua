-- Plotting

-- Use this function to perform your initial setup
function setup()
    scaleFactors = {} --We use a table so that is is passed as a reference instead of by value
    parameter.number("gravityScale", 0, 10, 1, function(x) scaleFactors[1] = x end)
    sin = funcObject(function(x) return 100*math.sin(0.1*x) end, color(255))
    linear = funcObject(function(x) return 5*x end, color(255))
    -- Create a dynamic plot using the gravity vector 
    dynamic1 = funcObject(
    function()
        return gravityScale*100*Gravity.x
    end, color(255, 0, 4, 255), true)
      dynamic2 = funcObject(
    function()
        return gravityScale*100*Gravity.y
    end, color(12, 255, 0, 255), true)
    dynamic3 = funcObject(
    function()
        return gravityScale*100*Gravity.z
    end, color(0, 112, 255, 255), true)
  
    ax = Axes(0.5*WIDTH, 0.5*HEIGHT)
    ax:add_plot(sin)
    ax:add_plot(linear)
    ax:add_plot(dynamic1)
    ax:add_plot(dynamic2)
    ax:add_plot(dynamic3)

end

-- This function gets called once every frame
function draw()
    -- This sets a dark background color 
    background(40, 40, 50)

    -- This sets the line thickness
    strokeWidth(5)
    ax:run()
    --Do your drawing here
    
end

