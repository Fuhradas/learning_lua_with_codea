funcObject = class()

function funcObject:init(_func, _color, _isDynamic)
    self.func = _func --function that will be used to compute the value
    -- Dynamics funcObjects change with time and as a consequence they need a buffer
    self.isDynamic = _isDynamic or false
    --Create the buffer
    if(self.isDynamic) then
        self.buffer = {}
        -- The bufferSize will be overwriten by the Axes class as soon as a dynamic function
        -- is appended into the dynamicPlots table
        self.bufferSize = 100 --initialize bufferSize
    end
    self.color = _color or color(255, 0, 0, 255)
end

function funcObject:draw()
    -- Codea does not automatically call this method
end

function funcObject:value(x)
    return self.func(x)
end

function funcObject:touched(touch)
    -- Codea does not automatically call this method
end

--newer points are inserted at the end of the table, so points with lower indexes (table keys) are "older"
--then those with higher ones
function funcObject:update_buffer()
    if(self.isDynamic) then
        if( tableLength(self.buffer) > self.bufferSize) then
            table.remove(self.buffer, 1) --remove the first point (remeber that indexes in lua start in 1
            table.insert(self.buffer, self:value()) -- insert current position
        else --if maxSize has not been reached yet
            --add new point at the end (default position if we call insert with no additional arguments)
            table.insert(self.buffer, self:value())
        end
    end
end

function tableLength(T)
  local count = 0
  for _ in pairs(T) do count = count + 1 end
  return count
end