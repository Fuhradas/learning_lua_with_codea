
-- Sockets
-- ######### CLIENT ########

--[[
function setup()
    serverIP = "localhost"
    socket = require("socket")
    client = socket.tcp()
    -- connect to the Python server Socket
    client:connect(serverIP, "8888")
    -- Create a table to test how it is received by the remote socket
    testTable = {roll = 1.23, pitch = 4.56, yaw = 7.89 }
    client:send(json.encode(testTable))
    parameter.action("Send data", send_data)
    timer1s = Timer(1)
end

function draw()
    timer1s:tick()
    if(timer1s:alarm()) then
        send_data()
    end
end

function send_data()
    print("Sending data to"..serverIP)
    local socket = require("socket")
    local client = socket.tcp()
    -- connect to the Python server Socket
    client:connect(serverIP, "8888")
    --print(client:getpeername())
    local Table = {roll = Gravity.x, pitch = Gravity.y, yaw = Gravity.z }
    client:send(json.encode(Table))
 
end
]]


-- ######### SERVER ########
function setup()
    -- gets a tcp master object
    socket = require("socket")
    --create a socket object
    server = socket.tcp()
    --bind it to the given IP and port
    server:bind("localhost", 8888)
    --Turn the master object into a server (a call to bind must have been done before)
    server:listen()
    -- I don't really know what does this method do...
    server:settimeout(0)
    conn = nil
end

function draw()
    -- The server accept method returns a client
    -- conn stands for connection
    local conn = server:accept()
    if(conn) then
        pattern, error, data = conn.receive(conn)
        print(data)
    end
end