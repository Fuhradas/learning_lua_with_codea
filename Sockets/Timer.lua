Timer = class()

-- Due to the limited resolution of os.time(), the minimum time interval is 1 second
-- os.clock() is very innacurate

function Timer:init(_interval)
    -- you can accept and set parameters here
    self.interval = _interval --interval between flags in seconds
    self.flag = false -- flag to indicate that an interval has been completed
    self.t = os.time() -- time were the flag was triggered
end

-- makes the Timer class run
function Timer:tick()
    if(os.time() - self.t >= self.interval) then
        self.flag = true
        self.t = os.time()
    else
        self.flag = false
    end
end

-- returns the flag
function Timer:alarm()
    return self.flag
end
